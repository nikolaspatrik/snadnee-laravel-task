<!DOCTYPE html>
<html>
<head>
<title>Products</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    @isset ($error_msg)
    <div class="row mt-4"> 
        <div class="col">
        </div>

        <div class="col">
            <div class="alert alert-danger" role="alert">
            {{ $error_msg }}
            </div>
        </div>

        <div class="col">
        </div>
    </div>
    @endisset
    <div class="container mt-5">
        <div class="d-flex justify-content-center mb-3">
            @if (!$box_id)
            <a href="/" class="btn btn-secondary btn-lg active me-5" role="button" aria-pressed="true">Back to main page</a>
            <a href="/products/create" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Create Product</a>
            @else
            <a href="/boxes" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Back to boxes</a>
            @endif
        </div>
        <div class="d-flex justify-content-center mb-3">
            <form class="/products" method="GET">
                @csrf
                <input type="hidden" name="box_id" value="{{ $box_id ?? '' }}">
                <div class="row ">
                    <div class="col form-floating">
                        <input type="text" class="form-control" placeholder="Query" id="query "name="query">
                        <label for="query">Search string</label>
                    </div>
                    <div class="col form-floating">
                        <select class="form-select" id="query_type" name="query_type">
                            <option value="id">Id</option>
                            <option value="name">Name</option>
                            <option value="supplier">Supplier</option>
                        </select>
                        <label for="query_type">Search by</label>
                    </div>
                    <div class="col form-floating">
                        <select class="form-select" id="order_by" name="order_by">
                            <option value="id">Id</option>
                            <option value="edited">Date edit</option>
                        </select>
                        <label for="query">Order by</label>
                    </div>

                    <div class="col">
                        <input class="form-check-input" type="checkbox" name="in_stock" id="in_stock">
                        <label class="form-check-label" for="in_stock">
                            In Stock
                        </label>
                    </div>

                    <div class="col">
                        <input class="btn btn-primary" type="submit" value="Search">
                    </div>
                </div>
            <form>
        </div>
        <table class="table table-bordered mb-5">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Stock</th>
              <th scope="col">Add to Box</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($products as $product)
            <tr>
              <td>
                    @if (!$box_id)
                    <a href="/products/{{ $product->id }}" class="link-secondary">{{ $product->name }}</a>
                    @else
                    {{ $product->name }}
                    @endif
              </td>
              <td>{{ $product->price }} €</td>
              <td>{{ $product->stock }}</td>
              <td>
                @if ($box_id)
                <a href="/boxes/{{ $box_id }}/product/{{ $product->id }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Add Product</a>
                @endif
              </td>
            </tr>
            @empty
                <td> <h6> No products.  </h6> </td>
            @endforelse
          </tbody>
        </table>

        <div class="d-flex justify-content-center">
            {!! $products->links() !!}
        </div>

    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>
</html>
