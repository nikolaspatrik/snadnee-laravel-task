<!DOCTYPE html>
<html>
<head>
    <title>Show Product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <div class="d-flex justify-content-center"> 
            <a href="/products" class="btn btn-secondary btn-lg active mb-3 me-3" role="button" >Back to products page</a>
        </div>
        <div class="row">
            <div class="col">
            </div> 
            <div class="col">
                <img class="img-fluid rounded mt-5" src="{{ $photo }}">
                <div class="container row mt-3">
                <div class="col">
                    <form action="/products/{{ $id }}" method="POST">
                    @method('DELETE')
                        @csrf
                        <input class="btn btn-danger" type="submit" value="Delete">
                    </form>
                </div> 
                <div class="col">
                <form action="/products/{{ $id }}/edit">
                    @csrf
                    <input class="btn btn-primary" type="submit" value="Edit">
                </form>
                </div> 
                </div> 

                <ul class="list-group mt-2">
                    <li class="list-group-item active">{{ $name }}</li>
                    <li class="list-group-item">Stock: {{ $stock }}</li>
                    <li class="list-group-item">Price: {{ $price }}</li>
                    <li class="list-group-item">Supplier: {{ $supplier }}</li>
                </ul>
            </div>
            <div class="col">
            </div> 
        </div> 
        <div class="row mt-5">
            <div class="col">
            </div> 
            <div class="col">
                <h6>Boxes:</h6>
                <ul class="list-group mt-2">
                    @forelse ($boxes as $box)
                        <li class="list-group-item ">{{ $box->id}}: {{ $box->content}}</li>
                    @empty
                        <p>No boxes</p>
                    @endforelse
                </ul>
            </div> 
            <div class="col">
            </div> 
        </div>
    </div>
</body>
</html>
