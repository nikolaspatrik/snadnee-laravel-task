<!DOCTYPE html>
<html>
<head>
<title>Products</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <div class="d-flex justify-content-center"> 
            <a href="/" class="btn btn-secondary btn-lg active mb-3 me-3" role="button" >Welcome page</a>
            <a href="/boxes/create" class="btn btn-secondary btn-lg active mb-3" role="button" >Create Box</a>
        </div>
        <table class="table table-bordered mb-5">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Discount</th>
              <th scope="col">Final Price</th>
              <th scope="col">Content</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @forelse ($boxes as $box)
            <tr>
                <td>{{ $box->id }} </td>
                <td>{{ $box->discount }} %</td>
                <td>{{ $box->price}} €</td>
                <td>{{ $box->content}}</td>
                <td>
                    <form action="/boxes/{{ $box->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-primary" value="Buy" name="button">
                    </form>
                </td>
            </tr>
            @empty
                <td> <h6> No products.  </h6> </td>
            @endforelse
          </tbody>
        </table>

    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>
</html>
