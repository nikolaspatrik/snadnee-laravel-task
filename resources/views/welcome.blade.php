<!DOCTYPE html>
<html>
<head>
<title>Snadnee task</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
<div class="d-flex justify-content-center mt-5">
    <div class="col"></div>
    <div class="col-3 me-3">
        <div class="card bg-dark text-white">
          <a href="/boxes/"><img src="{{ asset('images/boxes.jpeg') }}" class="card-img"></a>
          <div class="card-img-overlay">
            <a href="/boxes/"><h5 class="card-title">Boxes</h5></a>
          </div>
        </div>
    </div>
    <div class="col-3 ms-3">
        <div class="card bg-dark text-white">
          <img src="{{ asset('images/products.jpeg') }}" class="card-img">
          <div class="card-img-overlay">
            <a href="/products/"><h5 class="card-title">Products</h5></a>
          </div>
        </div>
    </div>
    <div class="col"></div>
</div>
</style>
</body>
</html>
