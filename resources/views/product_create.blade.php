<!DOCTYPE html>
<html>
<head>
<title>Product</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
<div class="container mt-5">
    @if ($errors->any())
    <div class="row"> 
        <div class="col">
        </div>

        <div class="col">
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col">
        </div>
    </div>
    @endif
    @if ($current_photo)
    <div class="row mt-5"> 
        <div class="col">
        </div>

        <div class="col">
            <img src="{{ $current_photo }}" class="rounded img-thumbnail">
        </div>

        <div class="col">
        </div>
    </div>
    @endif

  <div class="row mt-5">
    <div class="col">
    </div>
    <div class="col">
    <form action="{{ $action }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if ($edit)
            @method('PUT')
        @endif
        <label for="id">Product id:</label><br>
        <input class="form-control" type="text" id="id" name="id" value="{{ $id }}" disabled><br>

        <label for="name">Product name:</label><br>
        <input class="form-control" type="text" name="name" value="{{ $name }}"><br>

        <label for="stock">Stock:</label><br>
        <input class="form-control" type="text" id="stock" name="stock" value="{{ $stock }}"><br>

        <label for="price">Price:</label><br>
        <input class="form-control" type="text" id="price" name="price" value="{{ $price }}"><br>

        <label for="photo">Photo:</label><br>
        <input class="form-control" type="file" id="photo" name="photo"><br>

        <label for="supplier">Supplier:</label><br>
        <select class="form-select" id="supplier" name="supplier">
            @foreach ($suppliers as $supplier)
                <option value="{{ $supplier->name }}" @if ($supplier->name == $current_supplier) selected @endif >{{ $supplier->name }}</option>
            @endforeach
        </select><br>

        <input class="btn btn-primary" type="submit" value="Submit">
    </form>

    </div>
    <div class="col">
    </div>
  </div>
</div>
</body>
</html>
