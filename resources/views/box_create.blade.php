<!DOCTYPE html>
<html>
<head>
<title>Box create</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
<div class="container mt-5">
    @if ($errors->any())
    <div class="row"> 
        <div class="col">
        </div>

        <div class="col">
            <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
        </div>

        <div class="col">
        </div>
    </div>
    @endif
    <div class="row mt-5">
    <div class="col">
    </div>
    <div class="col">
    <form action="/boxes" method="POST" >
        @csrf
        <label for="discount">Box Discount</label><br>
        <input class="form-control" type="text" id="discount" name="discount" ><br>

        <input class="btn btn-primary" type="submit" value="Add Products">
    </form>

    </div>
    <div class="col">
    </div>
  </div>
</div>
</body>
</html>
