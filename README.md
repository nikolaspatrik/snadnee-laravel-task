# SNADNEE Laravel task

[link na zadanie](https://drive.google.com/file/d/1146Q8DO8yEeP-240GzbjroBcASdZ21aA/view)

## Instalace

Spustite nasledujuce prikazy v korenovem adresare projektu:

```
composer install
alias sail=./vendor/bin/sail
sail up
sail artisan migrate:fresh --seed  # Vytvori schemu databazi a naseeduje data pre otestovanie
```

Nasledne v prehliadaci bude bezat applikacia na adresse `localhost:80/`

## Popis funkcionality

### '/' welcome page

Na welcome page si vyberiete medzi pracou s produktami a bedynkami.

### '/boxes'

Zoznam bedynek s popisom ich obsahu. Mozete vytvorit bedynku.
Pri vytvarani bedynky zadate zlavu pre bedynku a nasledne vas presmeruje na `/products` view.

Na tomto view je zobrazenie vsetkych produktov s formularom na filtrovanie/hladanie ako bolo napisane v zadani.
Kazdy z tychto produktov lze pridavat teraz ku predtym vytvaranej bedynke.


### '/products'

View ktory zobrazuju produkty a umoznuje ich vyhladavat/filtrovat.
Mozme si zobrazovat jednotlive produkty v detailoch a je vidiet bedynky v ktorych su obsiahnute.
V tomto detailnom je mozno dany produkt upravit alebo vymazat.

## Rozhodnutia pri vyvoji.

### Bedynky pridavanie do DB bez productov a produkty v dalsiom view.

Zoznam bedynek nezobrazi prazdne bedynky ale lze ich bohuzial vytvorit ( simple fix: vymazanie pri navrate
k zoznamu bedynok) <- je to skarede riesenie ale rozhodol som sa ho pouzit.

Avsak rozhodol som sa pre toto riesenie lebo som si nestihol nastudovat layouty pre blady templates a teda
ma napadla ina moznost ktora to nevyzaduje avsak,
riesenie ktore by to robilo "spravne" nie je user friendly a to zadavat produkty po nazvoch/idckach do textoveho pols pri zadavani zlavy.


### Boostrap a velmi jednoduche kostry

Predpokladal som ze tento task neni skuskou mojich dizajnersky schopnosti takze frontend je velmi jednoduchy
a taktiez ak by ste chceli vidiet krajsi fronted mozem vas odkazat na moj iny projekt.

### Nekomentoval som kod

Uprimne Laravel je velmi pekny framework (tento task bol mojim zoznamenim s nim) a kazdy riadok kodu mi pride ako sebedokumentujuci sa. Nechcel som nasilu davat komenty ked viem ze skusenim vyvojarom s Laravelom by nemal problem
citat tento kod. A pre istotu som tento kod reviewol v MR requestoch takze viem ze by sa mal lahko citat. 
HTML su na tom horsie za to sa ospravedlnujem (blame VIM for that lebo moje vyvojove prostredie vo Vime je setupnute na Python development a nie frontend :( a resetup vimu je zdlhava vec :D )



