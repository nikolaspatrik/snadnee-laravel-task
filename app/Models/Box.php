<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function getPriceAttribute()
    {
        $price = 0.0;
        foreach ($this->products()->get() as $product) {
            $price += $product->price;
        }
        return $price * (100 - $this->discount) / 100;
    }

    public function getContentAttribute()
    {
        $ids_counts = array();
        $ids_names = array();
        foreach($this->products()->get() as $product) {
            if (!isset($ids_counts[$product->id] )) {
                $ids_counts[$product->id] = 1;
                $ids_names[$product->id] = $product->name;
            } else {
                $ids_counts[$product->id] += 1;
            }
        }

        $content = '';
        foreach ($ids_counts as $id => $count) {
            $content .= $ids_names[$id] . ' ' . $count . 'x, ';
        }
        return $content;
    }
}
