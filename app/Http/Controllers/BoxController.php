<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxes = Box::has('products')->get();
        return view('boxes_index', ['boxes' => $boxes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('box_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['discount' => 'required|integer|between:0,100']);

        $box = new Box;

        $box->discount = $request->discount;
        $box->save();

        return redirect()->route('products.index', ['box_id' => $box->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function destroy(Box $box)
    {
        Log::info('deleting ' . $box->id);
        $box->delete();
        return redirect()->route('boxes.index');
    }

    public function add_product(Box $box, Product $product)
    {
        if($product->stock > 0) {
            $product->stock -= 1;
            $product->save();
            $box->products()->attach($product->id);
            return redirect()->route('products.index', ['box_id' => $box->id]);
        } else {
            return redirect()->route(
                'products.index',
                [
                    'box_id' => $box->id,
                    'error_msg' => 'Out Of Stock'
                ]
            );
        }
    }
}
