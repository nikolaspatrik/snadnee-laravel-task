<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Supplier;
use App\Http\Requests\StoreProductRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $box_id = $request->input('box_id', null);
        $error_msg = $request->input('error_msg', null);

        $products = Product::class;
        $products_per_page = 5;
        if ($request->has('query')) {
            $query_type = $request->input('query_type');
            $query_val = $request->input('query', '');
            if ($query_val) {
                if ($query_type === 'supplier') {
                    Log::info('Querying for supplier ' .  $query_val);
                    $products = Product::whereHas(
                        'supplier',
                        function (Builder $query) use ($query_val) {
                            $query->where('name', 'like', '%'. $query_val .'%');
                        }
                    );
                } else if ($query_type === 'name') {
                    Log::info('Querying for product name' .  $query_val);
                    $products = Product::where('name', 'like', '%'. $query_val .'%');
                } else {
                    Log::info('Querying for id' .  $query_val);
                    $products = Product::where('id', '=', $query_val);
                }
            } else {
                Log::info('Querying all');
                $products = DB::table('products');
            }

            $order_by = $request->input('order_by');
            if ($order_by === 'edited') {
                $products = $products->orderBy('updated_at', 'desc');
            } else {
                $products = $products->orderBy('id', 'asc');
            }   

            if ($request->boolean('in_stock')) {
                $products->where('stock', '>', 0);
            }

            $products = $products->paginate($products_per_page);
        } else {
            Log::info('Querying ' .  $products_per_page . ' products');
            $products = Product::paginate($products_per_page);
        }


        return view(
            'product_index', 
            [
                'products' => $products, 
                'box_id' => $box_id,
                'error_msg' => $error_msg,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'product_create',
            [
                'action' => '/products/',
                'edit' => false,
                'id' => '',
                'name' => '',
                'stock' => '',
                'price' => '',
                'current_supplier' => '',
                'current_photo' => null,
                'suppliers' => Supplier::all(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = new Product;

        $product->name = $request->name;
        $product->stock = (int) $request->stock;
        $product->price = (float) $request->price;
        $path = $request->photo->store('images');
        $product->photo = $path;
        $product->supplier_id = Supplier::where('name', $request->supplier)->first()->id;

        $product->save();
        return redirect()->route('products.show', [$product]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view(
            'product_show',
            [
                'id' => $product->id,
                'name' => $product->name,
                'stock' => $product->stock,
                'price' => $product->price,
                'photo' => Storage::url($product->photo),
                'supplier' => $product->supplier->name,
                'boxes' => $product->boxes->unique('box_id'),
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view(
            'product_create',
            [
                'action' => '/products/' . $product->id,
                'edit' => true,
                'id' => $product->id,
                'name' => $product->name,
                'stock' => $product->stock,
                'price' => $product->price,
                'current_photo' => Storage::url($product->photo),
                'current_supplier' => $product->supplier->name,
                'suppliers' => Supplier::all(),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProductRequest $request, Product $product)
    {
        $product->name = $request->name;
        $product->stock = (int) $request->stock;
        if ($request->hasFile('photo')) {
            $path = $request->photo->store('images');
            $product->photo = $path;
        }
        $product->price = (float) $request->price;
        $product->supplier_id = Supplier::where('name', $request->supplier)->first()->id;

        $product->save();
        return redirect()->route('products.show', [$product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }
}
